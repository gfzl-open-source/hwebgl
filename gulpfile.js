/**
 * Created by MBENBEN on 2017/4/18.
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
gulp.task('clean',function(){
    gulp.src('lib').pipe(clean());
})
gulp.task('concatjs',function(){
    setTimeout(function(){
        gulp.src(['src/pano.js','src/unitl.js']).pipe(concat('hwebglpano.js')).pipe(gulp.dest('./lib'));
        gulp.src(['src/unitl.js','src/objview.js']).pipe(concat('hwebglobj.js')).pipe(gulp.dest('./lib'));
        gulp.src(['src/pano.js','src/unitl.js']).pipe(concat('hwebglpano.min.js')).pipe(uglify()).pipe(gulp.dest('./lib'));
        gulp.src(['src/unitl.js','src/objview.js']).pipe(concat('hwebglobj.min.js')).pipe(uglify()).pipe(gulp.dest('./lib'));
    },1000)
})
gulp.task('default',['clean','concatjs'], function() {
    // 将你的默认的任务代码放在这
});